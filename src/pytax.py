"""
Implements the main functionality of PyTax
"""


class PyTax:
    """
    This class implements PyTax
    """

    def __init__(self, income: int, filing_status: str, brackets: dict) -> None:
        self.gross_income = income
        self.filing_status = filing_status
        self.bracket = brackets[filing_status]
        self.resources = brackets["source"]

    def pytax_driver(self) -> None:
        """
        Main driver for PyTax
        :return: None
        """
        print("Welcome to PyTax")

        print(
            f"\nGross Income: {self.gross_income:>12,.2f}"
            + f"\nFiling Status: {self.filing_status:>11}"
        )
        print("\nCalculating marginal tax schedule...")
        tax = self.calculate_marginal_tax()

        print(f"Total tax paid: {tax:,.2f}")

        print("\nCalculating effective tax ... ")
        self.calculate_effective_tax(tax)

        print(f"\nFor more information: {self.resources}")

    def calculate_marginal_tax(self) -> float:
        """
        Calculate and display marginal tax rate schedule
        :return: total tax paid
        :rtype: float
        """
        total_tax: float = 0
        taxed_income: float = 0

        for i in self.bracket:
            bracket_tax: float = 0.0

            if self.gross_income <= i["upper"] or i["upper"] == 0:
                bracket_tax = max(self.gross_income - i["lower"], 0)
            else:
                bracket_tax = i["upper"] - i["lower"] + 1

            tax = bracket_tax * i["rate"]
            taxed_income += bracket_tax
            total_tax += tax

            print(
                f"{i['rate'] * 100}% - Taxed: {bracket_tax:>10,.2f}"
                + f" Tax: {tax:>10,.2f}"
                + f" Income: {taxed_income:>10,.2f}"
                + f" Total Tax: {total_tax:>10,.2f}"
            )

        return total_tax

    def calculate_effective_tax(self, tax: float) -> None:
        """
        Calculate and display effective tax rate
        :param tax: total tax paid
        :return: None
        """
        print(f"Effective tax rate: {(tax / self.gross_income) * 100:.2f}%")
