# PyTax

[![python: v3.12](https://img.shields.io/badge/python-v3.12-blue)](https://www.python.org/)
[![code style: ruff](https://img.shields.io/badge/code%20style-ruff-000000.svg)](https://github.com/astral-sh/ruff)
[![license: MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/andeh575/pytax/-/blob/master/LICENSE)

A CLI written in `Python` to do quick tax bracket estimations.

## Usage

```txt
usage: PyTax CLI tax bracket tool [-h] [-v] -i INCOME [-f {single,joint}]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         Show application version
  -i INCOME, --income INCOME
                        Gross income (in whole dollars)
  -f {single,joint}, --filing-status {single,joint}
                        Tax filing status
```

## Initial setup

Install `pip`, `pipx`, `pipenv`:

```sh
# Example is for Ubuntu/Debian

sudo apt-get install python3-pip
python3 -m pip install --user pipx

# Add the following line to your shell configuration file
export PATH=$PATH:~/.local/bin

# Install packages with `pipx` to isolate them from the system python
pipx install pdm
```

Install packages into the environment:

```sh
pdm install --dev
```

## Development

Run the application with the following:

```sh
pdm run python src/pytaxcli.py
```

### Ruff Formatter

This repository uses [ruff](https://docs.astral.sh/ruff/) as a formatter and linter.
Follow the [installation guide](https://docs.astral.sh/ruff/installation/) to get
setup.

## Resources

- [Source: IRS 2020 Tax Adjustments](https://www.irs.gov/newsroom/irs-provides-tax-inflation-adjustments-for-tax-year-2020)
- [Source: IRS 2021 Tax Adjustments](https://www.irs.gov/newsroom/irs-provides-tax-inflation-adjustments-for-tax-year-2021)
- [Source: IRS 2022 Tax Adjustments](https://www.irs.gov/newsroom/irs-provides-tax-inflation-adjustments-for-tax-year-2022)
- [Source: IRS 2023 Tax Adjustments](https://www.irs.gov/newsroom/irs-provides-tax-inflation-adjustments-for-tax-year-2023)
- [Source: IRS 2024 Tax Adjustments](https://www.irs.gov/newsroom/irs-provides-tax-inflation-adjustments-for-tax-year-2024)
