"""
PyTax CLI tool
"""
from argparse import ArgumentParser, Namespace
from constants import __version__
from pytax import PyTax
import json
import os


def load_tax_year(tax_year: int) -> dict:
    """
    Load the tax brackets for a provided year
    :return: tax brackets
    :rtype: dict
    """
    target_dir = f"resources/{tax_year}"
    brackets = {}

    print(f"Loading {tax_year} brackets...")

    for file in os.listdir(target_dir):
        with open(os.path.join(target_dir, file), "r") as f:
            brackets.update(json.loads(f.read()))

    return brackets


def process_arguments() -> Namespace:
    """
    Process command line arguments
    :return: parsed_arguments
    :rtype: obj
    """
    parser = ArgumentParser(prog="PyTax CLI tax bracket tool")

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s " + __version__,
        help="Show application version",
    )
    parser.add_argument(
        "-i",
        "--income",
        action="store",
        dest="income",
        type=int,
        required=True,
        help="Gross income (in whole dollars)",
    )
    parser.add_argument(
        "-f",
        "--filing-status",
        action="store",
        dest="filing_status",
        choices=["single", "joint"],
        default="single",
        help="Tax filing status",
    )
    parser.add_argument(
        "--tax-year",
        action="store",
        dest="tax_year",
        type=int,
        default=2021,
        help="Tax year used for calculation",
    )

    parsed_args = parser.parse_args()

    return parsed_args


# Receive arguments from the parser
ARGS = process_arguments()

# Instantiate the pytax tool
taxes = PyTax(
    income=ARGS.income,
    filing_status=ARGS.filing_status,
    brackets=load_tax_year(ARGS.tax_year),
)

# Execute the tool
taxes.pytax_driver()
